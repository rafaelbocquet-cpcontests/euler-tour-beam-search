#pragma once
#include "header.hpp"

/*
 * The type `state<N>` is the type of states for the (N²-1)-puzzle.
 *
 */
template<i32 N>
struct state {
  // `hx` and `hy` are the current coordinates of the "hole" of the puzzle.
  i32 hx, hy;

  /* `data` is a 2-dimensional array describing the current state of the puzzle.
   *
   * The solved state is described by the following array:
   *     0  1  2  3
   *     4  5  6  7
   *     8  9 10 11
   *    12 13 14 15
   *
   * The number 0 corresponds to the hole of the puzzle.
   */
  i32 data[N][N];

  /* `score` is the current heuristic value of the puzzle: the sum of the
   * manhattan distance from every puzzle piece to its target position.
   *
   * The score is zero if and only if the puzzle is in a solved state.
   *
   * The manhattan distance is not a very good heuristic value for the
   * (N²-1)-puzzle, but it is very simple to compute.
   */
  i32 score;

  state() = default;

  /*
   * Resets the puzzle to its solved state.
   */
  void reset(){
    hx = hy = 0;
    FOR(x,N) FOR(y,N) data[x][y] = x*N+y;
    score = 0;
  }

  /* `is_valid_move(d)` checks whether moving the hole in the direction `d` is valid.
   * The direction `d` is an integer in [0..3]:
   *       0
   *       ^
   *    3<   >1
   *       v
   *       2
   */
  FORCE_INLINE
  bool is_valid_move(int d) {
    if(d == 0) return hx > 0;
    else if(d == 1) return hy < N-1;
    else if(d == 2) return hx < N-1;
    else if(d == 3) return hy > 0;
    else return false;
  }

  /* `move_score(d)` returns the score after moving the hole in the direction
   * `d`, without actually performing the move.
   *
   * Assumes that `d` is a valid move.
   */
  FORCE_INLINE
  i32 move_score(int d) {
    if(d == 0) {
      i32 tmp = data[hx-1][hy];
      i32 tmpx = tmp/N;
      return score + abs(tmpx-hx) - abs(tmpx-(hx-1));
    }else if(d == 1) {
      i32 tmp = data[hx][hy+1];
      i32 tmpy = tmp%N;
      return score + abs(tmpy-hy) - abs(tmpy-(hy+1));
    }else if(d == 2) {
      i32 tmp = data[hx+1][hy];
      i32 tmpx = tmp/N;
      return score + abs(tmpx-hx) - abs(tmpx-(hx+1));
    }else if(d == 3) {
      i32 tmp = data[hx][hy-1];
      i32 tmpy = tmp%N;
      return score + abs(tmpy-hy) - abs(tmpy-(hy-1));
    }else{
      return 0;
    }
  }

  /* `do_move(d)` moves the hole in the direction `d`.
   *
   * Assumes that `d` is a valid move.
   *
   * For performance reasons, `do_move(d)` should preferably be called when `d`
   * is a compile time constants, so as to avoid branching on `d`. This can be
   * accomplished by unrolling a for loop:
   *
   *   UNROLL_FOR4(d) {
   *     state.do_move(d);
   *     // do something with the updated state here
   *     state.undo_move(d);
   *   }
   */
  FORCE_INLINE
  void do_move(int d) {
    if(d == 0) {
      i32 tmp = data[hx-1][hy];
      i32 tmpx = tmp/N;
      score += abs(tmpx-hx) - abs(tmpx-(hx-1));
      data[hx][hy] = tmp;
      data[hx-1][hy] = 0;
      hx -= 1;
    }else if(d == 1) {
      i32 tmp = data[hx][hy+1];
      i32 tmpy = tmp%N;
      score += abs(tmpy-hy) - abs(tmpy-(hy+1));
      data[hx][hy] = tmp;
      data[hx][hy+1] = 0;
      hy += 1;
    }else if(d == 2) {
      i32 tmp = data[hx+1][hy];
      i32 tmpx = tmp/N;
      score += abs(tmpx-hx) - abs(tmpx-(hx+1));
      data[hx][hy] = tmp;
      data[hx+1][hy] = 0;
      hx += 1;
    }else if(d == 3) {
      i32 tmp = data[hx][hy-1];
      i32 tmpy = tmp%N;
      score += abs(tmpy-hy) - abs(tmpy-(hy-1));
      data[hx][hy] = tmp;
      data[hx][hy-1] = 0;
      hy -= 1;
    }
  }

  /* `undo_move(d)` reverts a move in direction `d`, by moving in the opposite
   * direction `d^2`.
   */
  FORCE_INLINE
  void undo_move(int d) {
    do_move(d^2);
  }

  /* `compute_score()` recomputes the score of the puzzle state.
   * This is used to check that the value of the variable `score` is correct.
   */
  i32 compute_score() {
    i32 true_score = 0;
    FOR(x,N) FOR(y,N) if(data[x][y] != 0) {
      int vx = data[x][y]/N;
      int vy = data[x][y]%N;
      true_score += abs(vx-x);
      true_score += abs(vy-y);
    }
    return true_score;
  }

  /* `compute_parity()` computes the parity of a puzzle state.
   * Only puzzle states with an even parity are solvable.
   */
  i32 compute_parity() {
    i32 p = 0;
    FOR(x1,N) FOR(y1,N) FOR(x2,N) FOR(y2,N)  {
      if(x1*N+y1 >= x2*N+y2) continue;
      if(data[x1][y1] > data[x2][y2]) p ^= 1;
    }
    p ^= (hx&1);
    p ^= (hy&1);
    return p;
  }

  /* `check()` checks that the current puzzle state is valid.
   */
  void check() {
    runtime_assert(compute_score() == score);
    runtime_assert(compute_parity() == 0);
  }

  /* `generate()` generates a uniformly sampled random valid puzzle state.
   */
  void generate() {
    vector<int> I(N*N);
    FOR(i,N*N) I[i] = i;
    do {
      rng.shuffle(I);
      FOR(x,N) FOR(y,N) data[x][y] = I[x*N+y];
      FOR(x,N) FOR(y,N) if(data[x][y] == 0) { hx = x; hy = y; }
    } while(compute_parity() != 0);
    score = compute_score();
  }

  void print(ostream& out = cout) {
    out << "+";
    FOR(j,N) out << (j ? "---" : "--");
    out << "+\n";
    FOR(i,N) {
      out << "|";
      FOR(j,N) {
        out << (j?" ":"");
        if(data[i][j] == 0) out << "__";
        else out << setw(2) << data[i][j];
      }
      out << "|\n";
    }
    out << "+";
    FOR(j,N) out << (j ? "---" : "--");
    out << "+\n" << flush;
    out << "Score = " << score << endl;
  }
};

/* The result of beam search, for benchmarking purposes.
 */
struct result {
  float  time_spent;
  size_t memory_usage;
};

/* All beam search functions will have the following signature:
 *
 *   result beam_search(state<N> const& initial_state,
 *                      int width,
 *                      int num_steps)
 *
 *   `initial_state` is the puzzle states that needs to be solved.
 *   `width`         is the beam width.
 *   `num_steps`     is the number of beam search iterations.
*/
