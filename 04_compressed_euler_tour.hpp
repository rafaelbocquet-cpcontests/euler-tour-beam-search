#pragma once
#include "header.hpp"
#include "state.hpp"
#include "range_coding.hpp"

namespace compressed_euler_tour {
  /*
   * In `compressed_euler_tour::beam_search`, we improve on the space-efficiency
   * of `euler_tour::beam_search`, using range coding (see range_coding.hpp).
   *
   * In the case of the (N²-1)-puzzle, we use 1 bit per edge to indicate whether
   * it is a down-edge or an up-edge, and 2 bits per down-edge to store the move
   * direction. This can be optimized to <2 bits per down-edge, by using the
   * fact that some move directions are not always valid.
   */

  using compressed_euler_tour = vector<uint8_t>;

  /*
   * The structure of the code is the same as for
   * `euler_tour::traverse_euler_tree`.
   *
   * We do not store any bit indicating whether a node is a
   * leaf. Instead, a node is a leaf if its depth is `leaf_depth`.
   */
  template<i32 N>
  void traverse_euler_tour
  (int leaf_depth,
   state<N> const& initial_state,
   compressed_euler_tour const& tree_in,
   compressed_euler_tour& tree_out,
   vector<i32>& histogram,
   i32 cutoff, float cutoff_keep_probability)
  {
    range_decoder in(tree_in.data());
    range_encoder out(tree_out.data());

    state<N> S = initial_state;
    vector<i16> stack_moves;
    int ncommit = 0;
    int depth = 0;

    while(1) {
      /*
       * range_encoder does not check for buffer overflows, we need to ensure
       * that tree_out is large enough.
       */
      if(out.size > 0.99 * tree_out.size()) {
        tree_out.resize(tree_out.size() * 1.2);
        out.ptr = tree_out.data() + out.size;
      }

      int is_down = in.get(2); in.update(is_down,1,2);
      if(is_down) {
        int data = in.get(4); in.update(data,1,4);
        stack_moves.emplace_back(data);
        S.do_move(data);
        depth += 1;
      }else{
        if(depth == leaf_depth) {
          if(S.score < cutoff || (S.score == cutoff && rng.randomFloat() < cutoff_keep_probability)) {
            while(ncommit < (i32)stack_moves.size()) {
              out.put(1,1,2);
              out.put(stack_moves[ncommit],1,4);
              ncommit += 1;
            }

            UNROLL_FOR4(d) if(S.is_valid_move(d)) {
              i32 v = S.move_score(d);
              histogram[v] += 1;
              out.put(1,1,2);
              out.put(d,1,4);
              out.put(0,1,2);
            }
          }
        }

        if(stack_moves.empty()) {
          out.put(0,1,2);
          out.finish();
          return;
        }

        i32 d = stack_moves.back();
        S.undo_move(d);
        depth -= 1;
        if(ncommit == (i32)stack_moves.size()) {
          out.put(0,1,2);
          ncommit -= 1;
        }
        stack_moves.pop_back();
      }
    }

    runtime_assert(false);
  }

  template<i32 N>
  result beam_search(state<N> const& initial_state,
                   int width,
                   int num_steps) {
    timer TM;

    const int max_score = 2*N*N*N;
    vector<int> histogram(max_score+1, 0);

    int buffer_size = 10000;
    compressed_euler_tour tour_current(buffer_size), tour_next(buffer_size);
    { range_encoder out(tour_current.data());
      out.put(0,1,2);
      out.finish();
    }

    i32   cutoff = max_score;
    float cutoff_keep_probability = 1.0;

    FOR(istep, num_steps) {
      histogram.assign(max_score+1,0);

      traverse_euler_tour(istep,
                          initial_state,
                          tour_current, tour_next,
                          histogram,
                          cutoff, cutoff_keep_probability);

      { i32 total_count = 0;
        cutoff = max_score;
        cutoff_keep_probability = 1.0;
        FOR(i,max_score+1) {
          if(total_count+histogram[i] > width) {
            cutoff = i;
            cutoff_keep_probability = (float)(width-total_count) / (float)histogram[i];
            break;
          }
          total_count += histogram[i];
        }
      }

      swap(tour_current, tour_next);
    }

    return result {
      .time_spent   = TM.elapsed(),
      .memory_usage = vector_memory_usage(tour_current) + vector_memory_usage(tour_next),
    };
  }

}
