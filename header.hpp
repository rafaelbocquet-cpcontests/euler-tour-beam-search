#pragma once

#include <iostream>
#include <limits>
#include <memory>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <cmath>
#include <cassert>
#include <chrono>
#include <iomanip>
#include <bitset>

using namespace std;

// Macros

using i16 = int16_t;
using u16 = uint16_t;
using i32 = int32_t;
using u32 = uint32_t;
using i64 = int64_t;
using u64 = uint64_t;

#define FOR(i, n) for(i64 i = 0; i < (i64)(n); ++i)
#define FORU(i, j, k) for(i64 i = (j); i <= (i64)(k); ++i)
#define FORD(i, j, k) for(i64 i = (j); i >= (i64)(k); --i)

#define UNROLL_FOR8(i) _Pragma("GCC unroll 8") FOR(i,8)
#define UNROLL_FORD8(i) _Pragma("GCC unroll 8") FORD(i,7,0)
#define UNROLL_FOR4(i) _Pragma("GCC unroll 4") FOR(i,4)
#define UNROLL_FORD4(i) _Pragma("GCC unroll 4") FORD(i,3,0)

#define AS_STRING_IMPL(x) #x
#define AS_STRING(x) AS_STRING_IMPL(x)
#define runtime_assert(x) do { if(!(x)) { throw runtime_error(__FILE__ ":" AS_STRING(__LINE__) " Assertion failed: " #x); } } while(0)

#define ALWAYS_INLINE __attribute__((always_inline))
#define FORCE_INLINE ALWAYS_INLINE inline

struct RNG {
  u32 x, y, z, w;

  RNG(u64 seed) {
    reset(seed);
  }

  RNG() {
    reset(time(0));
  }

  inline void reset(u64 seed) {
    struct splitmix64_state {
      u64 s;

      u64 splitmix64() {
        u64 result = (s += 0x9E3779B97f4A7C15);
        result = (result ^ (result >> 30)) * 0xBF58476D1CE4E5B9;
        result = (result ^ (result >> 27)) * 0x94D049BB133111EB;
        return result ^ (result >> 31);
      }
    };

    splitmix64_state s { seed };

    x = (u32)s.splitmix64();
    y = (u32)s.splitmix64();
    z = (u32)s.splitmix64();
    w = (u32)s.splitmix64();
  }

  inline u32 xorshift128() {
    u32 t = x;
    t ^= t << 11;
    t ^= t >> 8;
    x = y; y = z; z = w;
    w ^= w >> 19;
    w ^= t;
    return w;
  }

  // UniformRandomBitGenerator interface
  using result_type = u32;
  constexpr u32 min(){ return numeric_limits<u32>::min(); }
  constexpr u32 max(){ return numeric_limits<u32>::max(); }
  u32 operator()() { return randomInt32(); }

  inline u32 randomInt32() {
    return xorshift128();
  }

  inline u64 randomInt64() {
    return (((u64)randomInt32())<<32ll) | ((u64)randomInt32());
  }

  inline u32 random32(u32 r) {
    return (((u64)randomInt32())*r)>>32;
  }

  inline u64 random64(u64 r) {
    return randomInt64()%r;
  }

  inline u32 randomRange32(u32 l, u32 r) {
    return l + random32(r-l+1);
  }

  inline u64 randomRange64(u64 l, u64 r) {
    return l + random64(r-l+1);
  }

  inline double randomDouble() {
    return (double)randomInt32() / 4294967296.0;
  }

  inline float randomFloat() {
    return (float)randomInt32() / 4294967296.0;
  }

  template<class T>
  void shuffle(vector<T>& v) {
    int sz = v.size();
    for(int i = sz; i > 1; i--) {
      int p = random32(i);
      swap(v[i-1],v[p]);
    }
  }

  template<class T>
  inline int sample_index(vector<T> const& v) {
    return random32(v.size());
  }

  template<class T>
  inline T sample(vector<T> const& v) {
    return v[sample_index(v)];
  }
} rng;

struct timer {
  chrono::high_resolution_clock::time_point t_begin;

  timer() {
    t_begin = chrono::high_resolution_clock::now();
  }

  void reset() {
    t_begin = chrono::high_resolution_clock::now();
  }

  float elapsed() const {
    return chrono::duration<float>(chrono::high_resolution_clock::now() - t_begin).count();
  }
};

template<class T>
size_t vector_memory_usage(vector<T> const& v) {
  /*
   * When computing the memory usage of a vector, I use v.size() instead of
   * v.capacity(). In a memory constrained environment, it is usually possible
   * to preallocate the vector to the correct size and avoid wasting memory.
   */
  return v.size() * sizeof(T);
}
