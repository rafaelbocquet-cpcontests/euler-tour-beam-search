#pragma once
#include "state.hpp"

namespace baseline {

  /*
   * `baseline::beam_search` is a reference beam search implementation.
   *
   * It maintains a vector `beam_current` of puzzle states that contains at most `width` states.
   *
   * At every beam search iteration, all children of these states are generated.
   * Since the branching factor is < 4, at most `4*width` new states are
   * generated.
   *
   * The vector `beam_current` is updated to the best `width` states among these
   * children.
   */

  template<i32 N>
  result beam_search(state<N> const& initial_state,
                     int width,
                     int num_steps) {
    timer TM;
    size_t max_memory = 0;

    /*
     * `beam_current` is a vector of puzzle states.
     */
    vector<state<N>> beam_current;

    /*
     * `beam_next` is a vector of puzzle states that is used to store the
     * children of the states in `beam_current`.
     */
    vector<state<N>> beam_next;

    /*
     * `beam_indices` is a permutation of [0 .. beam_next.size()-1] that is
     * partitioned when selecting the best `width` states in `beam_next`.
     *
     * This is an optimization: we could also partition `beam_next`, but
     * sizeof(i32) < sizeof(state<N>).
     */
    vector<i32>      beam_indices;

    // At first, `beam_current` only contains the initial state.
    beam_current.emplace_back(initial_state);

    // We perform `num_states` iterations.
    FOR(istep, num_steps) {

      FOR(ia, beam_current.size()) {
        // We iterate over all states in `beam_current`.
        state<N>& sa = beam_current[ia];

        // We generate all children of the state `sa` and add them to `beam_next`.
        UNROLL_FOR4(d) if(sa.is_valid_move(d)) {
          int ib = beam_next.size();
          beam_next.emplace_back();
          state<N>& sb = beam_next[ib];
          sb = sa;
          sb.do_move(d);
          beam_indices.emplace_back(ib);
        }
      }

      /* When more than `width` states have been generated, we partition and
       * resize `beam_indices` so that it contains the indices of the best
       * `width` states in `beam_next`.
       */
      if((i32)beam_indices.size() > width) {
        nth_element(begin(beam_indices), begin(beam_indices)+width, end(beam_indices),
                    [&](int i, int j){ return beam_next[i].score < beam_next[j].score; });
        beam_indices.resize(width);
      }

      max_memory = max(max_memory,
                       vector_memory_usage(beam_current) +
                       vector_memory_usage(beam_next) +
                       vector_memory_usage(beam_indices));

      // Finally, we update `beam_current` with the best `width` states from `beam_next`.
      beam_current.clear();
      for(int i : beam_indices) {
        beam_current.emplace_back(beam_next[i]);
      }

      beam_next.clear();
      beam_indices.clear();
    }

    return result {
      .time_spent   = TM.elapsed(),
      .memory_usage = max_memory,
    };
  }
}
