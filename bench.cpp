#include "header.hpp"
#include "state.hpp"
#include "00_baseline.hpp"
#include "01_histogram.hpp"
#include "02_history.hpp"
#include "03_euler_tour.hpp"
#include "04_compressed_euler_tour.hpp"

string print_memory(size_t mem) {
  ostringstream os;
  if(mem < 1'000) {
    os << mem << " B";
  }else if(mem < 1'000'000) {
    os << (mem / 1'000) << "." << (mem % 1'000) << " kB";
  }else {
    os << (mem / 1'000'000) << "." << (mem / 1'000 % 1'000) << " MB";
  }
  return os.str();
}

template<int N, class F>
void bench(vector<state<N>> const& initial_states,
           vector<i32> const& beam_widths,
           i32 num_steps,
           string const& name,
           F&& f) {
  auto num_tests = initial_states.size();

  cout << "## " << name << ": " << endl;

  for(i32 width : beam_widths) {
    float  total_time = 0;
    size_t total_memory = 0;
    for(auto const& s : initial_states) {
      auto result = f(s, width, num_steps);
      total_time   += result.time_spent;
      total_memory += result.memory_usage;
    }
    cout << " width = " << setw(6) << width
         << ", average time = " << setprecision(3) << fixed << total_time / num_tests
         << ", average memory = " << setw(10) << print_memory(total_memory / num_tests)
         << endl;
  }
}

template<int N>
void bench_with() {
  i32 num_tests = 10;
  i32 num_steps = 100;

  cout << "# N = " << N << ", num_tests = " << num_tests << endl;

  vector<state<N>> initial_states;
  FOR(i,num_tests) {
    initial_states.emplace_back();
    initial_states.back().generate();
  }

  vector<i32> beam_widths = {1'000, 10'000, 100'000};

  bench(initial_states, beam_widths, num_steps, "baseline", baseline::beam_search<N>);
  bench(initial_states, beam_widths, num_steps, "histogram", histogram::beam_search<N>);
  bench(initial_states, beam_widths, num_steps, "history", history::beam_search<N>);
  bench(initial_states, beam_widths, num_steps, "euler_tour", euler_tour::beam_search<N>);
  bench(initial_states, beam_widths, num_steps, "compressed_euler_tour", compressed_euler_tour::beam_search<N>);
}

int main(){
  rng.reset(42);
  bench_with<4>();
  bench_with<8>();
  bench_with<12>();
  bench_with<16>();
  return 0;
}
