#pragma once
#include "state.hpp"

namespace history {

  /*
   * An issue with `baseline::beam_search` and `histogram::beam_search` is that
   * they do not allow for the computation of the solution after the search
   * completes!
   *
   * In order to recompute the solution, we need to remember the history of
   * every state, i.e. a path between the state and the root of the search tree.
   *
   * This history is encoded as a linked list.
   */

  struct linked_list {
    /*
     * Here `value` is the (N²-1)-puzzle move direction that lead to the current state.
     */
    i32 value;

    /*
     * `prev_index` is the index of the tail of the linked list in the `history`
     * array, or -1 if the tail is empty.
     */
    i32 prev_index;
  };

  template<i32 N>
  struct state_with_history {
    state<N> S;
    i32      history_index;
  };

  template<i32 N>
  result beam_search(state<N> const& initial_state,
                     int width,
                     int num_steps) {
    timer TM;
    size_t max_memory = 0;

    const int max_score = 2*N*N*N;

    vector<linked_list> history;
    vector<int> histogram;

    vector<state_with_history<N>> beam_current;
    vector<state_with_history<N>> beam_next;

    beam_current.emplace_back(state_with_history<N> {
        .S = initial_state,
        .history_index = -1,
      });

    i32   cutoff = max_score;
    float cutoff_keep_probability = 1.0;

    FOR(istep, num_steps) {
      histogram.assign(max_score+1,0);

      FOR(ia, beam_current.size()) {
        state<N>& sa = beam_current[ia].S;
        i32 ha = beam_current[ia].history_index;

        if(sa.score > cutoff || (sa.score == cutoff && rng.randomFloat() > cutoff_keep_probability)) {
          continue;
        }

        UNROLL_FOR4(d) if(sa.is_valid_move(d)) {
          int ib = beam_next.size();
          beam_next.emplace_back();
          state<N>& sb = beam_next[ib].S;
          sb = sa;
          sb.do_move(d);

          /*
           * When creating a new state, we record its history.
           */
          beam_next[ib].history_index = history.size();
          history.emplace_back(linked_list {
              .value      = (i32)d,
              .prev_index = ha,
            });

          histogram[sb.score] += 1;
        }
      }

      { i32 total_count = 0;
        cutoff = max_score;
        cutoff_keep_probability = 1.0;
        FOR(i,max_score+1) {
          if(total_count+histogram[i] > width) {
            cutoff = i;
            cutoff_keep_probability = (float)(width-total_count) / (float)histogram[i];
            break;
          }
          total_count += histogram[i];
        }
      }

      max_memory = max(max_memory,
                       vector_memory_usage(history) +
                       vector_memory_usage(beam_current) +
                       vector_memory_usage(beam_next));

      swap(beam_current,beam_next);
      beam_next.clear();
    }

    /* At the end of the search, we can reconstruct the sequence of
     * moves leading to the best state in `beam_current`.
     */
    if(!beam_current.empty()) {
      auto const& sa = *min_element(begin(beam_current), end(beam_current),
                                    [&](auto const& sa, auto const& sb){ return sa.score < sb.score; });
      vector<i32> moves;
      for(i32 hindex = sa.history; hindex != 1; hindex = history[hindex].prev_index){
        moves.push_back(history[hindex].value);
      }
      reverse(begin(moves), end(moves));
      /* `moves` now contains the sequence of moves leading to the
       * state `sa`.
       */ 
    }

    return result {
      .time_spent   = TM.elapsed(),
      .memory_usage = max_memory,
    };
  }

}
