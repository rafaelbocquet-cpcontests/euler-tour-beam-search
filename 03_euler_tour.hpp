#pragma once
#include "header.hpp"
#include "state.hpp"

namespace euler_tour {

  /*
   * In `beam_search::history`, we can notice that the history of a state is
   * sufficient to recover the state. Indeed, we can traverse the history and
   * apply all of its move to the initial state. However this does not make
   * efficient use of the fact that there is a lot of sharing in the histories
   * of different states in the beam search.
   *
   * `euler_tour::beam_search` is an implementation of beam search that
   * maintains the Euler tour of the tree spanned by the histories of all nodes
   * in the current set of the beam search. In other words, instead of
   * remembering a set of states, we remember differences ("deltas") between
   * nodes. A difference is encoded by a path in the tree, that is as a sequence
   * of up-edges (undoing a move) following by a sequence of down edges (doing a
   * move).
   *
   * I originally came up with the idea of using the Euler tour while trying to
   * implement efficient garbage collection for histories in beam search.
   */

  /*
   * An `euler_tour_edge` represents an edge in the Euler tour of a tree.
   *
   * An edge can either be a down-edge, moving down from a node to its child, or
   * an up-edge, moving from a node to its parent.
   *
   * An up-edge can be a leaf, it means that it corresponds to a puzzle state
   * that is in the current set of the beam search.
   *
   * For down-edges, we remember the (N²-1)-puzzle move that corresponds to the
   * transition from parent to child in `data`.
   *
   * It is also possible to remember the score of leaves in `data`. This is not
   * needed for the (N²-1)-puzzle, because the score of a state can be computed
   * incrementally very easily.
   *
   * For many problems, this can be replaced by a single integer
   * (e.g. with positive values indicating down-edges and non-positive
   * values indicating up-edges).
   */
  struct euler_tour_edge {
    bool is_down;
    bool is_leaf;
    i16  data;
  };

  /*
   * An `euler_tour` is a list of edges.
   */
  using euler_tour = vector<euler_tour_edge>;

  /*
   * The function `traverse_euler_tour` implements a traversal of an Euler tour
   * to generate the Euler tour for the next step of the beam search.
   */
  template<i32 N>
  void traverse_euler_tour
  (state<N> const& initial_state,
   euler_tour const& tour_current,
   euler_tour& tour_next,
   vector<i32>& histogram,
   i32 cutoff, float cutoff_keep_probability)
  {
    // The traversal starts at the initial state.
    state<N> S = initial_state;

    /*
     * We keep a stack of moves that corresponds to the history of the current state `S`.
     */
    vector<i16> stack_moves;

    /*
     * The variable `ncommit` contains the size of the prefix of `stack_moves`
     * that has already been committed to the output tree `tour_next`.
     *
     * Suppose that we reach a leaf in the input tree, but the leaf is
     * discarded by the beam search. Then we do not want to include
     * the down-edges that lead to that leaf in the output tree. These
     * down-edges are kept as moves in `stack_moves`, but are not
     * commited.
     *
     */
    int ncommit = 0;

    FOR(iedge, (i32)tour_current.size()) {
      auto const& edge = tour_current[iedge];
      if(edge.is_down) {
        /*
         * When we see a down-edge, we apply its move to the current state and
         * push it to the top of `stack_moves`.
         *
         * Note that the move is not added to the output tree yet; and ncommit
         * is not incremented.
         */
        stack_moves.emplace_back(edge.data);
        S.do_move(edge.data);
      }else{

        /*
         * When we reach an up-edge, we first check whether it is a leaf.
         */
        if(edge.is_leaf) {
          /*
           * When reaching a leaf, we determing whether to keep it using the
           * same method as in `histogram::beam_search`.
           */
          if(edge.data < cutoff || (edge.data == cutoff && rng.randomFloat() < cutoff_keep_probability)) {
            /*
             * If the leaf is kept, we need to commit all of the uncommited
             * moves from `stack_moves`.
             */
            while(ncommit < (i32)stack_moves.size()) {
              tour_next.emplace_back(euler_tour_edge {
                  .is_down = 1,
                  .is_leaf = 0,
                  .data    = stack_moves[ncommit],
                });
              ncommit += 1;
            }

            /*
             * If we see a state with a low score, it may be the right
             * place to remember it and finish the search.
             *
             *  if(S.score == 0) {
             *    // A solution has been found, its history is in stack_moves.
             *  }
             */

            /*
             * We then add new leaves for every child of the current state.
             */
            UNROLL_FOR4(d) if(S.is_valid_move(d)) {
              i32 v = S.move_score(d);
              histogram[v] += 1;
              tour_next.emplace_back(euler_tour_edge {
                  .is_down = 1,
                  .is_leaf = 0,
                  .data    = (i16)d,
                });
              tour_next.emplace_back(euler_tour_edge {
                  .is_down = 0,
                  .is_leaf = 1,
                  .data    = (i16)v,
                });
            }
          }
        }

        /*
         * At this point we need to process the up-edge.
         */

        /*
         * If `stack_moves` is empty, the up-edge is the last edge in the Euler
         * tour, and marks the end of the traversal.
         */
        if(stack_moves.empty()) {
          tour_next.emplace_back(euler_tour_edge {
              .is_down = 0,
              .is_leaf = 0,
              .data = 0,
            });
          return;
        }

        // We undo the move at the top of `stack_moves`.
        i32 d = stack_moves.back();
        S.undo_move(d);

        /*
         * If the last move has been commited, we add the corresponding up-edge
         * to the output Euler tour.
         */
        if(ncommit == (i32)stack_moves.size()) {
          tour_next.emplace_back(euler_tour_edge {
              .is_down = 0,
              .is_leaf = 0,
              .data = 0,
            });
          ncommit -= 1;
        }

        // We pop the last move from the top of stack_moves.
        stack_moves.pop_back();
      }
    }

    /*
     * This can never be reached: this function can only by exited the return
     * statement above.
     */
    runtime_assert(false);
  }

  /*
   * Once `traverse_euler_tour` is defined, the definition of
   * `euler_tour::beam_search` is very similar to the previous beam search
   * functions.
   */
  template<i32 N>
  result beam_search(state<N> const& initial_state,
                     int width,
                     int num_steps) {
    timer TM;

    const int max_score = 2*N*N*N;
    vector<int> histogram(max_score+1, 0);

    euler_tour tour_current, tour_next;
    /*
     * We initialize `tour_current` to the Euler tour of the empty tree. We
     * still have an edge in this Euler tour, used to mark the end of the
     * traversal.
     */
    tour_current.emplace_back(euler_tour_edge{
        .is_down = 0,
        .is_leaf = 1,
        .data = 0,
      });

    i32   cutoff = max_score;
    float cutoff_keep_probability = 1.0;

    FOR(istep, num_steps) {
      histogram.assign(max_score+1,0);

      traverse_euler_tour(initial_state,
                          tour_current, tour_next,
                          histogram,
                          cutoff, cutoff_keep_probability);

      { i32 total_count = 0;
        cutoff = max_score;
        cutoff_keep_probability = 1.0;
        FOR(i,max_score+1) {
          if(total_count+histogram[i] > width) {
            cutoff = i;
            cutoff_keep_probability = (float)(width-total_count) / (float)histogram[i];
            break;
          }
          total_count += histogram[i];
        }
      }

      swap(tour_current, tour_next);
      tour_next.clear();
    }

    return result {
      .time_spent   = TM.elapsed(),
      .memory_usage = vector_memory_usage(tour_current) + vector_memory_usage(tour_next),
    };
  }

}
