#pragma once
#include "state.hpp"

namespace histogram {

  /*
   * `histogram::beam_search` is a perfomance improvement over
   * `baseline::beam_search`.
   *
   * Recomputing `beam_current` at every step from `beam_next` is expensive: the
   * implementation in `baseline::beam_search` uses random memory accesses that
   * are not cache efficient.
   *
   * Instead, we can filter states at the next beam search iteration.
   *
   * Typically, the range of possible scores is smaller than the beam width. We
   * can count of states for each score in an array `histogram`, indexed by the
   * possible scores. We can then use this array to determine two values `cutoff
   * : i32` and `cutoff_keep_probability : float`.
   *
   * At the next iteration, we process a state with score `s < cutoff`
   * with probability 1 and a state with score `s == cutoff` with
   * probability `cutoff_keep_probability`. The value of
   * `cutoff_keep_probability` is chosen so that the expected number
   * of processed states is the beam width.
   */

  template<i32 N>
  result beam_search(state<N> const& initial_state,
                     int width,
                     int num_steps) {
    timer TM;
    size_t max_memory = 0;

    /*
     * `max_score` is an upper bound on the score of a puzzle state.
     */
    const int max_score = 2*N*N*N;
    vector<int> histogram(max_score+1, 0);

    vector<state<N>> beam_current;
    vector<state<N>> beam_next;

    beam_current.emplace_back(initial_state);

    /*
     * On the first iteration, we always keep the initial state.
     */
    i32   cutoff = max_score;
    float cutoff_keep_probability = 1.0;
    FOR(istep, num_steps) {
      /*
       * The histogram array is reset.
       */
      histogram.assign(max_score+1,0);

      FOR(ia, beam_current.size()) {
        state<N>& sa = beam_current[ia];
        /*
         * We skip states depending on `cutoff` and `cutoff_keep_probability`.
         */
        if(sa.score > cutoff || (sa.score == cutoff && rng.randomFloat() > cutoff_keep_probability)) {
          continue;
        }

        UNROLL_FOR4(d) if(sa.is_valid_move(d)) {
          int ib = beam_next.size();
          beam_next.emplace_back();
          state<N>& sb = beam_next[ib];
          sb = sa;
          sb.do_move(d);

          /*
           * We increment the count for the score `sb.score`.
           */
          histogram[sb.score] += 1;
        }
      }

      /*
       * Computations of `cutoff` and `cutoff_keep_probability` for the next iteration.
       */
      { i32 total_count = 0;
        cutoff = max_score;
        cutoff_keep_probability = 1.0;
        FOR(i,max_score+1) {
          if(total_count+histogram[i] > width) {
            cutoff = i;
            cutoff_keep_probability = (float)(width-total_count) / (float)histogram[i];
            break;
          }
          total_count += histogram[i];
        }
      }

      max_memory = max(max_memory,
                       vector_memory_usage(beam_current) +
                       vector_memory_usage(beam_next));

      swap(beam_current,beam_next);
      beam_next.clear();
    }

    return result {
      .time_spent   = TM.elapsed(),
      .memory_usage = max_memory,
    };
  }

}
