# Euler tour beam search

Euler tour beam search is an implementation method for [beam search](https://en.wikipedia.org/wiki/Beam_search) that achieves a much better space-efficiency (depending, of course, on the precise problem being solved). 

It also avoids making copies of states. For problems with large state representations (say, over 1000 bits per state), this provides a large performance improvement, as the running time of beam search otherwise becomes dominated by the cost of copying states.

Instead of maintaining a set of candidate states, Euler tour beam search maintains the subtree of the search tree spanned by the candidate states.
Iterating through the set of candidate states can then be accomplished by traversing the tree.
Futhermore, the tree can be encoded compactly using its Euler tour.

I invented this method for Al Zimmermann's [Stepping Stones](http://azspcs.com/Contest/SteppingStones) programming contest ([my solution](https://gitlab.com/rafaelbocquet-cpcontests/steppingstones)).

A similar method has been described by rhoo [https://qiita.com/rhoo/items/f2be256cde5ad2e62dde (japanese)](https://qiita.com/rhoo/items/f2be256cde5ad2e62dde).

This repository contains several variants of beam search for the [(N²-1)-puzzle](https://en.wikipedia.org/wiki/15_puzzle), and benchmarking for the time and space efficiency of every variant.

The following files are heavily commented, and are intended to be read in order:
- [state.hpp](./state.hpp)
- [00_baseline.hpp](./00_baseline.hpp)
- [01_histogram.hpp](./01_histogram.hpp)
- [02_history.hpp](./02_history.hpp)
- [03_euler_tour.hpp](./03_euler_tour.hpp)
- [04_compressed_euler_tour.hpp](./compressed_euler_tour.hpp)

## Benchmark results

Each test runs 100 iterations of the specified beam search implementation, for 10 uniformly sampled instances of the (N²-1)-puzzle.

<details>
<summary>Click to see the benchmark results.</summary>

```
# N = 4, num_tests = 10
## baseline:
 width =   1000, average time = 0.006, average memory = 346.228 kB
 width =  10000, average time = 0.058, average memory =   3.419 MB
 width = 100000, average time = 0.844, average memory =  34.691 MB
## histogram:
 width =   1000, average time = 0.005, average memory = 512.673 kB
 width =  10000, average time = 0.053, average memory =   5.224 MB
 width = 100000, average time = 0.545, average memory =   51.29 MB
## history:
 width =   1000, average time = 0.004, average memory =   2.702 MB
 width =  10000, average time = 0.050, average memory =   27.35 MB
 width = 100000, average time = 0.535, average memory = 252.115 MB
## euler_tour:
 width =   1000, average time = 0.016, average memory = 122.850 kB
 width =  10000, average time = 0.140, average memory =   1.142 MB
 width = 100000, average time = 1.324, average memory =  11.585 MB
## compressed_euler_tour:
 width =   1000, average time = 0.023, average memory =    20.0 kB
 width =  10000, average time = 0.194, average memory = 169.840 kB
 width = 100000, average time = 1.745, average memory =   1.621 MB
# N = 8, num_tests = 10
## baseline:
 width =   1000, average time = 0.008, average memory =   1.246 MB
 width =  10000, average time = 0.082, average memory =  12.591 MB
 width = 100000, average time = 1.970, average memory = 125.571 MB
## histogram:
 width =   1000, average time = 0.006, average memory =   1.941 MB
 width =  10000, average time = 0.109, average memory =  19.390 MB
 width = 100000, average time = 1.487, average memory = 193.446 MB
## history:
 width =   1000, average time = 0.007, average memory =   4.308 MB
 width =  10000, average time = 0.126, average memory =  42.995 MB
 width = 100000, average time = 1.648, average memory = 424.435 MB
## euler_tour:
 width =   1000, average time = 0.014, average memory =  57.356 kB
 width =  10000, average time = 0.132, average memory = 561.992 kB
 width = 100000, average time = 1.278, average memory =   5.634 MB
## compressed_euler_tour:
 width =   1000, average time = 0.019, average memory =    20.0 kB
 width =  10000, average time = 0.181, average memory =  85.992 kB
 width = 100000, average time = 1.667, average memory =  874.23 kB
# N = 12, num_tests = 10
## baseline:
 width =   1000, average time = 0.010, average memory =   2.782 MB
 width =  10000, average time = 0.287, average memory =   28.28 MB
 width = 100000, average time = 3.622, average memory = 278.433 MB
## histogram:
 width =   1000, average time = 0.009, average memory =   4.346 MB
 width =  10000, average time = 0.293, average memory =  43.453 MB
 width = 100000, average time = 2.823, average memory = 431.239 MB
## history:
 width =   1000, average time = 0.009, average memory =   6.268 MB
 width =  10000, average time = 0.307, average memory =  63.358 MB
 width = 100000, average time = 3.011, average memory = 631.173 MB
## euler_tour:
 width =   1000, average time = 0.015, average memory =  62.164 kB
 width =  10000, average time = 0.145, average memory = 627.407 kB
 width = 100000, average time = 1.344, average memory =   6.254 MB
## compressed_euler_tour:
 width =   1000, average time = 0.021, average memory =    20.0 kB
 width =  10000, average time = 0.194, average memory =   93.14 kB
 width = 100000, average time = 1.849, average memory = 906.223 kB
# N = 16, num_tests = 10
## baseline:
 width =   1000, average time = 0.015, average memory =   4.962 MB
 width =  10000, average time = 0.551, average memory =  49.666 MB
 width = 100000, average time = 5.869, average memory = 495.442 MB
## histogram:
 width =   1000, average time = 0.012, average memory =   7.716 MB
 width =  10000, average time = 0.478, average memory =  77.498 MB
 width = 100000, average time = 4.690, average memory = 771.783 MB
## history:
 width =   1000, average time = 0.012, average memory =    9.40 MB
 width =  10000, average time = 0.513, average memory =  92.595 MB
 width = 100000, average time = 4.678, average memory = 944.743 MB
## euler_tour:
 width =   1000, average time = 0.014, average memory =  66.686 kB
 width =  10000, average time = 0.123, average memory = 612.803 kB
 width = 100000, average time = 1.243, average memory =    6.93 MB
## compressed_euler_tour:
 width =   1000, average time = 0.019, average memory =  20.400 kB
 width =  10000, average time = 0.169, average memory =  86.278 kB
 width = 100000, average time = 1.705, average memory =  874.23 kB
```

</details>

## Remarks

* All implementations can probably be made 2 times faster by avoiding
  memory allocations, bounds checking, etc.
  
